const Responses = require('../common/API_Responses');

exports.handler = async event => {
    console.log('event', event);

    if (!event.pathParameters || !event.pathParameters.ID) {
        return Responses._400({ message: 'Missing the ID from the path' });
    }

    let ID = event.pathParameters.ID;

    if (data[ID]) {
        return Responses._200(data[ID]);
    }

    return Responses._400({ message: 'No ID on the data' });
};

const data = {
    1234: { name: 'Nadim', age: 47, job: 'developer' },
    5678: { name: 'Clair', age: 56, job: 'architect' },
};
