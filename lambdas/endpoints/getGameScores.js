const Responses = require('../common/API_Responses');
const Dynamo = require('../common/Dynamo');

const tableName = process.env.tableName;

exports.handler = async event => {
    console.log('event', event);

    if (!event.pathParameters || !event.pathParameters.game) {
        return Responses._400({ message: 'Missing the game from the path' });
    }

    const game = event.pathParameters.game;

    const gamePlayers = await Dynamo.query({
        tableName,
        index: 'game-index',
        queryKey: 'game',
        queryValue: game,
    }).catch(err => {
        console.log('Error in Dynamo query', err);
        return null;
    });

    if (!gamePlayers) {
        return Responses._400({ message: 'Failed to query gamePlayers by game' });
    }

    return Responses._200({ gamePlayers });
};
