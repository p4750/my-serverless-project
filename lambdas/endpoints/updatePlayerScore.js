const Responses = require('../common/API_Responses');
const Dynamo = require('../common/Dynamo');

const tableName = process.env.tableName;

exports.handler = async event => {
    console.log('event', event);

    if (!event.pathParameters || !event.pathParameters.ID) {
        return Responses._400({ message: 'Missing the ID from the path' });
    }

    let ID = event.pathParameters.ID;

    const { score } = JSON.parse(event.body);

    const res = await Dynamo.update({
        tableName,
        primaryKey: 'ID',
        primaryKeyValue: ID,
        updateKey: 'score',
        updateValue: score,
    }).catch(err => {
        console.log('Error in Dynamo update', err);
        return null;
    });

    if (!res) {
        return Responses._400({ message: 'Failed to update user by ID' });
    }

    return Responses._200({});
};
